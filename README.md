# app-oaserver

#### 项目介绍

智能办公OA系统[SpringCloud-快速开发平台]，适用于医院，学校，中小型企业等机构的管理。Activiti5.22+动态表单实现零java代码即可做到复杂业务的流程实施，同时包含文件在线操作、日志、考勤、CRM、项目、拖拽式生成问卷、日程、笔记、计划、行政等多种复杂业务功能。同时，可进行授权二开。</br>

- erp+生产演示视频：https://www.bilibili.com/video/BV1yA411e7mm/
- 项目交流群：(群一(满)：[696070023](http://shang.qq.com/wpa/qunwpa?idkey=e9aace2bf3e05f37ed5f0377c3827c6683d970ac0bcc61b601f70dc861053229))(群二：[836039567](https://shang.qq.com/wpa/qunwpa?idkey=7bb6f29b27f772aadca9c7c4e384f7833c64e9c3c947b5e946c7b303d1fe174a))(群三：[887391486](https://shang.qq.com/wpa/qunwpa?idkey=a65f2e0292eb1048bb13abb7adca302bd83e3465974861ec1f04c2f7fffc4d99))`项目持续更新，欢迎进群讨论`
- 需要进微信群的，进微信群需要支付五元费用，为了防止发广告的等，望谅解。请加我微信：wzq_598748873
- 注：开源社区版只限学习，切勿使用此版本商用，内设授权码，默认十天删除所有非基础数据
- 系统新增传统风格界面，layui左菜单右内容风格
- 该项目只作为接口服务端
- 如果需要学习资料的（不免费，不贵，一杯咖啡的钱），可以加我微信。
- 不会搭建环境的，可以出钱让作者帮忙搭建，一次100，先付。
- 环境搭建资料获取：进下这个百度网盘群(手机端搜索入群)，3676101838

|项目|地址|
|-------|-------|
|主项目地址|https://gitee.com/doc_wei01/skyeye|
|APP端接口微服务地址|https://gitee.com/doc_wei01/app-oaserver|

#### 介绍

- 服务监控中心Spring Boot Admin；地址：http://localhost:8100
- 服务注册中心Spring Eureka；地址：http://localhost:8000/
- 网关zuul
- 熔断监控Hystrix；地址：http://localhost:8005
- API接口文档；地址：http://localhost:8888/doc.html
- 统一配置中心；地址：http://localhost:8900

#### 模块及端口

##### 配置
|模块|端口|模块|端口|
|-------|-------|-------|-------|
|服务注册中心|8000|服务监控中心|8100|
|网关zuul|8888|熔断监控Hystrix|8005|
|配置中心|8900| | |

##### 消费者
|模块|端口|模块|端口|
|-------|-------|-------|-------|
|总模块【app-consumer】|7001|售后服务模块【app-consumer-seal-service】|7002|

##### 生产者
|模块|端口|模块|端口|
|-------|-------|-------|-------|
|登录模块【app-pro-user】|9004|系统管理模块【app-pro-sys】|9005|
|行政模块【app-pro-adminstra】|9006|工作日志模块【app-pro-jobdiary】|9007|
|笔记模块【app-pro-note】|9008|便签模块【app-pro-sticky-notes】|9009|
|文件管理（云盘）模块【app-pro-disk-cloud】|9010|售后服务模块【app-pro-seal-service】|9011|

#### 服务器部署注意事项

1.ActiveMQ链接地址、账号、密码的修改<br />
2.Redis集群的修改<br />
3.MySQL数据库链接地址、账号、密码的修改<br />
4.图片资源路径存储的修改<br />

#### 本地开发环境搭建

- windows搭建nginx负载均衡（[下载](https://download.csdn.net/download/doc_wei/11010749)）
- windows搭建activemq单机版（[下载](https://download.csdn.net/download/doc_wei/11010746)）
- windows搭建redis集群（[下载](https://download.csdn.net/download/doc_wei/11010741)）

#### 技术选型

##### 后端技术:

技术|名称|官网
---|---|---
SpringCloud|核心框架|https://springcloud.cc/
MyBatis|ORM框架|http://www.mybatis.org/mybatis-3/zh/index.html
Druid|数据库连接池|https://github.com/alibaba/druid
Maven|项目构建管理|http://maven.apache.org/
redis|key-value存储系统|https://redis.io/
Activiti|工作流引擎|https://www.activiti.org/
Quartz 2.2.2|定时任务|http://www.quartz-scheduler.org/
ActiveMQ|消息队列|http://activemq.apache.org/replicated-leveldb-store.html
solr|企业级搜索应用服务器|https://lucene.apache.org/solr/

#### 效果图

| 效果图 | 效果图 | 效果图 |
| ------ | ------ | ------ |
| ![输入图片说明](https://images.gitee.com/uploads/images/2020/0103/224635_56784b6b_1541735.png "在这里输入图片标题") |![输入图片说明](https://images.gitee.com/uploads/images/2020/0103/232018_527da844_1541735.png "在这里输入图片标题")|![输入图片说明](https://images.gitee.com/uploads/images/2020/0103/232057_82da7f47_1541735.png "在这里输入图片标题")|


### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。
| 支付宝 | 微信 |
| ------ | ---- |
| ![      ](https://images.gitee.com/uploads/images/2019/1016/094014_96f92c56_1541735.png "微信截图_20191016093832.png") | ![     ](https://images.gitee.com/uploads/images/2019/1016/094025_65ba24f0_1541735.png "微信截图_20191016093850.png")|
